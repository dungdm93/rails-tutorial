# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Users
User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name:  "Đặng Minh Dũng",
             email: "dungdm93@example.com",
             password:              "qwerty",
             password_confirmation: "qwerty",
             activated: true,
             activated_at: Time.zone.now)

# Below user is unactivated
# It's used for test
User.create!(name:  "abc xyz",
             email: "abcxyz@gmail.com",
             password:              "123456",
             password_confirmation: "123456")

18.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
              email: email,
              password:              password,
              password_confirmation: password,
              activated: true,
              activated_at: Time.zone.now)
end

# Microposts
users = User.order(:created_at).take(7)
titles = Array.new(100) { rand(3) != 0 ? Faker::Lorem.sentence(1, false, 5).first(45) : nil }
contents = Array.new(100) { Faker::Lorem.paragraph(1, false, 3).first(200) }

inserts = Array.new(300) {
  user    = users.sample
  title   = titles.sample
  content = contents.sample
  user.microposts.new(title: title, content: content)
}
Micropost.transaction do
  inserts.each do |insert|
    insert.save
  end
end

# Comments
microposts = Micropost.order(created_at: :desc).take(10)
inserts = Array.new(100) {
  user      = users.sample
  micropost = microposts.sample
  content   = contents.sample
  Comment.new(user_id: user.id, micropost_id: micropost.id, content: content)
}
Comment.transaction do
  inserts.each do |insert|
    insert.save
  end
end

# Following relationships
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
