class StaticPagesController < ApplicationController

  def home
    if logged_in?
      @feed_items   = current_user.feed.paginate(page: params[:page], per_page: 15)
      @show_comment = false

      @micropost    = Micropost.new
    else
      @feed_items = Micropost.all.paginate(page: params[:page], per_page: 15)
    end
  end

  def help
  end

  def about
  end

  def contact
  end
end
