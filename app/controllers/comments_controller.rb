class CommentsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy

public

  def create
    @comment = current_user.comments.build(comment_params)
    @comment.micropost_id = params[:micropost_id]
    if @comment.save
      flash[:success] = "Comment is created."
      redirect_to request.referrer || root_url
    else
      # Bad code :(
      @micropost = Micropost.find(params[:micropost_id])
      @comments  = @micropost.comments.paginate(page: params[:page], per_page: 5)
      @show_comment = true
      render 'microposts/show'
    end
  end

  def destroy
    @comment.destroy
    flash[:success] = "Comment is deleted!"
    redirect_to request.referrer || root_url
  end

private

include SessionsHelper

  def comment_params
    params.require(:comment).permit(:content)
  end

  def correct_user
    @comment = Comment.find_by(id: params[:id])
    redirect_to root_url and return if @comment.nil?
    unless current_user?(@comment.user) || current_user?(@comment.micropost.user)
      redirect_to root_url
    end
  end
end
