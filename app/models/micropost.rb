class Micropost < ActiveRecord::Base

  belongs_to :user
  has_many :comments, dependent: :destroy
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader

  before_validation :strip_whitespace
  validates :user_id, presence: true
  validates :title, length: { maximum: 45 }
  validates :content, presence: true, length: { maximum: 200 }
  validate  :picture_size

private

  # Validates the size of an uploaded picture.
  def picture_size
    if picture.size > 5.megabytes
      errors.add(:picture, "should be less than 5MB")
    end
  end

  # trim whitespaces
  def strip_whitespace
    # squish method removes white space on both ends of the string
    # and groups multiple white space to single space.
    # str.squish equal to str.gsub(/\s+/, " ").strip
    self.title   = self.title.blank?   ? nil : self.title.squish
    self.content = self.content.blank? ? nil : self.content.strip
  end
end
