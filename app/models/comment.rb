class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :micropost
  default_scope -> { order(created_at: :desc) }

  before_validation :strip_whitespace
  validates :user_id, presence: true
  validates :micropost_id, presence: true
  validates :content, presence: true, length: { maximum: 200 }

private

  # trim whitespaces
  def strip_whitespace
    self.content = self.content.blank? ? nil : self.content.strip
  end
end
