require 'test_helper'

class MicropostsShowTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @user      = users(:michael)
    @micropost = microposts(:orange)
    @other     = users(:archer)
  end
  
  test "micropost display" do
    get micropost_path(@micropost)
    assert_template 'microposts/show'
    assert_match @micropost.title, full_title(@micropost.title)

    assert_select 'h3', text: "Micropost ##{@micropost.id}"
    assert_select 'a>img.gravatar'
    assert_match @micropost.user.name, response.body
    assert_match @micropost.title, response.body
    assert_match @micropost.content, response.body
  end
end
