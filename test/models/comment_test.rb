require 'test_helper'

class CommentTest < ActiveSupport::TestCase

  def setup
    @user      = users(:michael)
    @micropost = microposts(:orange)
    @comment   = Comment.new(user_id: @user.id, micropost_id: @micropost.id, content: "Lorem ipsum")
  end

  test "should be valid" do
    assert @micropost.valid?
  end

  test "user id should be present" do
    @comment.user_id = nil
    assert_not @comment.valid?
  end

  test "micropost id should be present" do
    @comment.micropost_id = nil
    assert_not @comment.valid?
  end

  test "content should be present " do
    @comment.content = "   "
    assert_not @comment.valid?
  end

  test "content should be at most 200 characters" do
    @comment.content = "a" * 250
    assert_not @comment.valid?
  end

  test "content should be striped after save" do
    # striped content
    @micropost.update_attributes content: "   qwerty   123456.   "
    assert_equal "qwerty   123456.", @micropost.content
  end

  # test "order should be most recent first" do
  #   assert_equal Comment.first, comments(:most_recent)
  # end
end
