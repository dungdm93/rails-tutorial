require 'test_helper'

class MicropostTest < ActiveSupport::TestCase

  def setup
    @user = users(:michael)
    @micropost = @user.microposts.build(content: "Lorem ipsum")
  end

  test "should be valid" do
    assert @micropost.valid?
  end

  test "user id should be present" do
    @micropost.user_id = nil
    assert_not @micropost.valid?
  end

  test "content should be present " do
    @micropost.content = "   "
    assert_not @micropost.valid?
  end

  test "content should be at most 200 characters" do
    @micropost.content = "a" * 250
    assert_not @micropost.valid?
  end

  test "title should be at most 45 characters" do
    @micropost.title = "a" * 100
    assert_not @micropost.valid?
  end

  test "title and content should be striped after save" do
    # blank title
    @micropost.update_attributes title: "           "
    assert_equal nil, @micropost.title
    # squished title
    @micropost.update_attributes title: "   abc   xyz   "
    assert_equal "abc xyz", @micropost.title
    # striped content
    @micropost.update_attributes content: "   qwerty   123456.   "
    assert_equal "qwerty   123456.", @micropost.content
  end

  test "order should be most recent first" do
    assert_equal Micropost.first, microposts(:most_recent)
  end

  test "associated comments should be destroyed" do
    @micropost.save
    comment = Comment.create!(user_id: @user.id, micropost_id: @micropost.id, content: "Lorem ipsum")
    assert_difference 'Comment.count', -1 do
      @micropost.destroy
    end
  end
end
